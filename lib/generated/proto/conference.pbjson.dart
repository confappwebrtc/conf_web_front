///
//  Generated code. Do not modify.
//  source: conference.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use conferenceDescriptor instead')
const Conference$json = const {
  '1': 'Conference',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'title', '3': 2, '4': 1, '5': 9, '10': 'title'},
    const {'1': 'description', '3': 3, '4': 1, '5': 9, '10': 'description'},
    const {'1': 'image', '3': 4, '4': 1, '5': 12, '10': 'image'},
    const {'1': 'starts', '3': 5, '4': 1, '5': 3, '10': 'starts'},
    const {'1': 'ends', '3': 6, '4': 1, '5': 3, '10': 'ends'},
    const {'1': 'topic', '3': 7, '4': 1, '5': 9, '10': 'topic'},
    const {'1': 'status', '3': 8, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'email', '3': 9, '4': 1, '5': 9, '10': 'email'},
    const {'1': 'phone', '3': 10, '4': 1, '5': 9, '10': 'phone'},
    const {'1': 'telegram', '3': 11, '4': 1, '5': 9, '10': 'telegram'},
    const {'1': 'instagram', '3': 12, '4': 1, '5': 9, '10': 'instagram'},
    const {'1': 'vk', '3': 13, '4': 1, '5': 9, '10': 'vk'},
  ],
};

/// Descriptor for `Conference`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List conferenceDescriptor = $convert.base64Decode('CgpDb25mZXJlbmNlEg4KAmlkGAEgASgJUgJpZBIUCgV0aXRsZRgCIAEoCVIFdGl0bGUSIAoLZGVzY3JpcHRpb24YAyABKAlSC2Rlc2NyaXB0aW9uEhQKBWltYWdlGAQgASgMUgVpbWFnZRIWCgZzdGFydHMYBSABKANSBnN0YXJ0cxISCgRlbmRzGAYgASgDUgRlbmRzEhQKBXRvcGljGAcgASgJUgV0b3BpYxIWCgZzdGF0dXMYCCABKAlSBnN0YXR1cxIUCgVlbWFpbBgJIAEoCVIFZW1haWwSFAoFcGhvbmUYCiABKAlSBXBob25lEhoKCHRlbGVncmFtGAsgASgJUgh0ZWxlZ3JhbRIcCglpbnN0YWdyYW0YDCABKAlSCWluc3RhZ3JhbRIOCgJ2axgNIAEoCVICdms=');
@$core.Deprecated('Use conferencesDescriptor instead')
const Conferences$json = const {
  '1': 'Conferences',
  '2': const [
    const {'1': 'conferences', '3': 1, '4': 3, '5': 11, '6': '.conferencepackage.Conference', '10': 'conferences'},
  ],
};

/// Descriptor for `Conferences`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List conferencesDescriptor = $convert.base64Decode('CgtDb25mZXJlbmNlcxI/Cgtjb25mZXJlbmNlcxgBIAMoCzIdLmNvbmZlcmVuY2VwYWNrYWdlLkNvbmZlcmVuY2VSC2NvbmZlcmVuY2Vz');
@$core.Deprecated('Use sectionDescriptor instead')
const Section$json = const {
  '1': 'Section',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'title', '3': 2, '4': 1, '5': 9, '10': 'title'},
    const {'1': 'description', '3': 3, '4': 1, '5': 9, '10': 'description'},
  ],
};

/// Descriptor for `Section`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List sectionDescriptor = $convert.base64Decode('CgdTZWN0aW9uEg4KAmlkGAEgASgJUgJpZBIUCgV0aXRsZRgCIAEoCVIFdGl0bGUSIAoLZGVzY3JpcHRpb24YAyABKAlSC2Rlc2NyaXB0aW9u');
@$core.Deprecated('Use sectionsDescriptor instead')
const Sections$json = const {
  '1': 'Sections',
  '2': const [
    const {'1': 'sections', '3': 1, '4': 3, '5': 11, '6': '.conferencepackage.Section', '10': 'sections'},
  ],
};

/// Descriptor for `Sections`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List sectionsDescriptor = $convert.base64Decode('CghTZWN0aW9ucxI2CghzZWN0aW9ucxgBIAMoCzIaLmNvbmZlcmVuY2VwYWNrYWdlLlNlY3Rpb25SCHNlY3Rpb25z');
@$core.Deprecated('Use panelDescriptor instead')
const Panel$json = const {
  '1': 'Panel',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'title', '3': 2, '4': 1, '5': 9, '10': 'title'},
    const {'1': 'description', '3': 3, '4': 1, '5': 9, '10': 'description'},
    const {'1': 'starts', '3': 4, '4': 1, '5': 3, '10': 'starts'},
    const {'1': 'ends', '3': 5, '4': 1, '5': 3, '10': 'ends'},
    const {'1': 'section', '3': 6, '4': 1, '5': 11, '6': '.conferencepackage.Section', '10': 'section'},
    const {'1': 'user', '3': 7, '4': 1, '5': 11, '6': '.conferencepackage.User', '10': 'user'},
    const {'1': 'rating', '3': 8, '4': 1, '5': 1, '10': 'rating'},
    const {'1': 'status', '3': 9, '4': 1, '5': 9, '10': 'status'},
  ],
};

/// Descriptor for `Panel`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List panelDescriptor = $convert.base64Decode('CgVQYW5lbBIOCgJpZBgBIAEoCVICaWQSFAoFdGl0bGUYAiABKAlSBXRpdGxlEiAKC2Rlc2NyaXB0aW9uGAMgASgJUgtkZXNjcmlwdGlvbhIWCgZzdGFydHMYBCABKANSBnN0YXJ0cxISCgRlbmRzGAUgASgDUgRlbmRzEjQKB3NlY3Rpb24YBiABKAsyGi5jb25mZXJlbmNlcGFja2FnZS5TZWN0aW9uUgdzZWN0aW9uEisKBHVzZXIYByABKAsyFy5jb25mZXJlbmNlcGFja2FnZS5Vc2VyUgR1c2VyEhYKBnJhdGluZxgIIAEoAVIGcmF0aW5nEhYKBnN0YXR1cxgJIAEoCVIGc3RhdHVz');
@$core.Deprecated('Use panelsDescriptor instead')
const Panels$json = const {
  '1': 'Panels',
  '2': const [
    const {'1': 'panels', '3': 1, '4': 3, '5': 11, '6': '.conferencepackage.Panel', '10': 'panels'},
  ],
};

/// Descriptor for `Panels`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List panelsDescriptor = $convert.base64Decode('CgZQYW5lbHMSMAoGcGFuZWxzGAEgAygLMhguY29uZmVyZW5jZXBhY2thZ2UuUGFuZWxSBnBhbmVscw==');
