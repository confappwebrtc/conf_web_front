import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:webrtc_conf/providers/conf_provider.dart';
import 'package:webrtc_conf/styles.dart';
import 'package:webrtc_conf/widgets/current_user.dart';

class MainLayout extends ConsumerStatefulWidget {
  final Widget child;
  const MainLayout({Key? key, required this.child}) : super(key: key);

  @override
  _MainLayoutState createState() => _MainLayoutState();
}

class _MainLayoutState extends ConsumerState<MainLayout> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        flexibleSpace: Container(
            decoration:
                BoxDecoration(gradient: styles.panelDecoration.gradient)),
        title: DefaultTextStyle(
            style: Theme.of(context)
                .textTheme
                .headline5!
                .copyWith(color: styles.themeColors.onBrandText),
            child: Row(children: [
              Container(
                  padding: const EdgeInsets.only(right: 50.0),
                  child: Image.asset(
                    "assets/images/logo.png",
                    width: 210,
                    color: Colors.white,
                  )),
              // const Text("ConfApp"),
              // Padding(
              //   padding: const EdgeInsets.symmetric(horizontal: 8.0),
              //   child: Icon(Icons.arrow_forward_ios_rounded,
              //       size: 20, color: styles.themeColors.onBrandText),
              // ),
              //Text(_users.title),
              const Spacer(),
              const CurrentUserButton()
            ])),
      ),
      body: Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Expanded(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            child: AnimatedSwitcher(
                duration: Common.animationDuration, child: widget.child),
          ),
        )
      ]),
    );
  }
}
