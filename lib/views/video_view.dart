import 'package:flutter/material.dart';
import 'package:webrtc_conf/widgets/logo.dart';
import 'package:webrtc_conf/widgets/video_block.dart';
import 'package:webrtc_conf/widgets/video_block_list.dart';
import 'package:webrtc_conf/views/confVideo_view.dart';

import '../styles.dart';

class VideoView extends StatefulWidget {
  const VideoView({Key? key}) : super(key: key);

  @override
  _VideoViewState createState() => _VideoViewState();
}

class _VideoViewState extends State<VideoView> {
  Color selectedColor = styles.themeColors.brand;
  Color unselectedColor = styles.themeColors.caption;

  IconData volumeIcon = Icons.volume_up_rounded;
  IconData videoIcon = Icons.videocam_rounded;

  bool volumeOn = false;
  bool videoOn = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 20),
          child: Stack(alignment: Alignment.topLeft, children: [
            Padding(
              padding: const EdgeInsets.all(25),
              child: Center(child: VideoBlock()),
            ),
            //const LogoWidget(),
            // Positioned(
            //     right: 0,
            //     top: 0,
            //     bottom: 0,
            //     width: MediaQuery.of(context).size.height * 0.15 * 16 / 9,
            //     child: const VideoBlockList())
          ]),
        ),
        floatingActionButtonLocation:
            FloatingActionButtonLocation.miniCenterFloat,
        floatingActionButton: Container(
          height: 60,
          constraints: const BoxConstraints(minWidth: 400, maxWidth: 800),
          width: MediaQuery.of(context).size.width * 0.75,
          decoration: BoxDecoration(
              color: styles.themeColors.cardBackground,
              borderRadius: BorderRadius.circular(100)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              IconButton(
                  onPressed: () {
                    setState(() {
                      volumeOn = !volumeOn;
                      if (volumeOn) {
                        volumeIcon = Icons.volume_off_rounded;
                      } else {
                        volumeIcon = Icons.volume_up_rounded;
                      }
                    });
                  },
                  icon: Icon(volumeIcon,
                      color: volumeOn
                          ? styles.themeColors.caption
                          : styles.themeColors.brand)),
              IconButton(
                  onPressed: () {
                    setState(() {
                      videoOn = !videoOn;
                      if (videoOn) {
                        videoIcon = Icons.videocam_off_rounded;
                      } else {
                        videoIcon = Icons.videocam_rounded;
                      }
                    });
                  },
                  icon: Icon(videoIcon,
                      color: videoOn
                          ? styles.themeColors.caption
                          : styles.themeColors.brand)),
              IconButton(
                  onPressed: () {},
                  icon: const Icon(Icons.screen_share_rounded)),
              IconButton(
                  onPressed: () {}, icon: const Icon(Icons.group_rounded)),
              IconButton(
                  onPressed: () {
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ConfVideoView()));
                  },
                  icon: const Icon(Icons.call_end_rounded, color: Colors.red)),
            ],
          ),
        ));
  }
}
