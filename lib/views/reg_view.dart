import 'dart:html';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:webrtc_conf/providers/user_provider.dart';
import 'package:webrtc_conf/styles.dart';
import 'package:webrtc_conf/views/confList_view.dart';
import 'package:webrtc_conf/views/login_view.dart';
import 'package:webrtc_conf/views/confAll_view.dart';

class RegView extends ConsumerStatefulWidget {
  const RegView({Key? key}) : super(key: key);

  @override
  _RegViewState createState() => _RegViewState();
}

class _RegViewState extends ConsumerState<RegView> {
  bool visib = false;
  bool confpass = false;
  final _emKey = GlobalKey<FormState>();
  final _passKey = GlobalKey<FormState>();
  final _confpassKey = GlobalKey<FormState>();

  final TextEditingController emailController = TextEditingController();
  final TextEditingController passController = TextEditingController();
  final TextEditingController confpassController = TextEditingController();

  @override
  void dispose() {
    emailController.dispose();
    passController.dispose();
    confpassController.dispose();
    super.dispose();
  }

  Widget build(BuildContext context) {
    return Material(
      child: Scaffold(
          appBar: AppBar(
              flexibleSpace: Container(
                  decoration:
                      BoxDecoration(gradient: styles.panelDecoration.gradient)),
              title: DefaultTextStyle(
                style: Theme.of(context)
                    .textTheme
                    .headline3!
                    .copyWith(color: styles.themeColors.onBrandText),
                child: Row(
                  children: [const Text("ConfApp")],
                ),
              )),
          extendBodyBehindAppBar: true,
          extendBody: true,
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  height: 515,
                  width: 455,
                  foregroundDecoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                    border: Border.all(
                        color: styles.themeColors.autoBorder, width: 2),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(bottom: 20),
                        child: Image.asset("assets/images/logo.png",
                            width: 250, height: 82.26),
                      ),
                      Container(
                        child: Form(
                          key: _emKey,
                          child: TextFormField(
                            controller: emailController,
                            inputFormatters: [
                              LengthLimitingTextInputFormatter(100)
                            ],
                            decoration: const InputDecoration(
                                labelText: "Email",
                                hintText: "example@examp.ru"),
                            validator: (email) =>
                                email!.isEmpty || email.contains('@') == false
                                    ? 'Enter a valid email'
                                    : null,
                          ),
                        ),
                        width: 400,
                        padding:
                            const EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 20.0),
                      ),
                      Container(
                        child: Form(
                            key: _passKey,
                            child: TextFormField(
                              controller: passController,
                              obscureText: !visib,
                              inputFormatters: [
                                LengthLimitingTextInputFormatter(100)
                              ],
                              decoration: InputDecoration(
                                  labelText: "Password",
                                  hintText: "Enter password",
                                  suffixIcon: IconButton(
                                    icon: visib
                                        ? Icon(Icons.visibility_off)
                                        : Icon(Icons.visibility),
                                    onPressed: () {
                                      visib = !visib;
                                      setState(() {});
                                    },
                                  )),
                              validator: (password) =>
                                  password != null && password.length < 8
                                      ? 'Enter min 8 character'
                                      : null,
                            )),
                        width: 400,
                        padding:
                            const EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                      ),
                      Container(
                        child: Form(
                          key: _confpassKey,
                          child: TextFormField(
                              controller: confpassController,
                              obscureText: !confpass,
                              inputFormatters: [
                                LengthLimitingTextInputFormatter(100)
                              ],
                              decoration: InputDecoration(
                                  labelText: "Confirm password",
                                  hintText: "Repeat password",
                                  suffixIcon: IconButton(
                                    icon: confpass
                                        ? Icon(Icons.visibility_off)
                                        : Icon(Icons.visibility),
                                    onPressed: () {
                                      confpass = !confpass;
                                      setState(() {});
                                    },
                                  )),
                              validator: (confpass) =>
                                  passController.text == confpassController.text
                                      ? null
                                      : "Data does not match"),
                        ),
                        width: 400,
                        padding:
                            const EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                      ),
                      Row(
                        children: [
                          Padding(
                            padding:
                                const EdgeInsets.fromLTRB(38.0, 10.0, 0.0, 0.0),
                            child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 24, horizontal: 24),
                                    textStyle: Theme.of(context)
                                        .textTheme
                                        .bodyText1!
                                        .copyWith(
                                            color:
                                                styles.themeColors.background)),
                                onPressed: () {
                                  if (_emKey.currentState!.validate() &&
                                      _passKey.currentState!.validate() &&
                                      _confpassKey.currentState!.validate()) {
                                    Navigator.pushReplacement(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                ConfAllView()));
                                  }
                                },
                                child: const Text("Sing Up")),
                          )
                        ],
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text("Already have login and password?",
                          textAlign: TextAlign.right),
                      Padding(
                        padding: const EdgeInsets.only(left: 10.0),
                        child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                textStyle:
                                    Theme.of(context).textTheme.bodyText2!,
                                primary: styles.themeColors.background),
                            onPressed: () {
                              Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => LoginView()));
                            },
                            child: const Text(
                              "Log in",
                              textAlign: TextAlign.center,
                              style: TextStyle(color: Colors.blue),
                            )),
                      )
                    ],
                  ),
                )
              ],
            ),
          )),
    );
  }
}
