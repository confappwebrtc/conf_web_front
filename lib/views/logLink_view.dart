import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:webrtc_conf/providers/user_provider.dart';
import 'package:webrtc_conf/styles.dart';
import 'package:webrtc_conf/views/confVideo_view.dart';

class LogLinkView extends ConsumerStatefulWidget {
  const LogLinkView({Key? key}) : super(key: key);

  @override
  _LogLinkViewState createState() => _LogLinkViewState();
}

class _LogLinkViewState extends ConsumerState<LogLinkView> {
  @override
  Widget build(BuildContext context) {
    final _formKey = GlobalKey<FormState>();
    final TextEditingController linkController = TextEditingController();
    final currentUser = ref.watch(userProvider.state);

    return Material(
        child: Scaffold(
            appBar: AppBar(
              flexibleSpace: Container(
                  decoration:
                      BoxDecoration(gradient: styles.panelDecoration.gradient)),
              title: DefaultTextStyle(
                  style: Theme.of(context)
                      .textTheme
                      .headline3!
                      .copyWith(color: styles.themeColors.onBrandText),
                  child: Row(children: [
                    const Text("ConfApp"),
                  ])),
            ),
            extendBodyBehindAppBar: true,
            extendBody: true,
            body: Center(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                        height: 515,
                        width: 455,
                        foregroundDecoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(30),
                            border: Border.all(
                                color: styles.themeColors.autoBorder,
                                width: 2)),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(bottom: 20.0),
                                child: Image.asset("assets/images/logo.png",
                                    width: 250, height: 82.26),
                              ),
                              Container(
                                child: Form(
                                    key: _formKey,
                                    child: TextFormField(
                                      controller: linkController,
                                      inputFormatters: [
                                        LengthLimitingTextInputFormatter(100)
                                      ],
                                      decoration: InputDecoration(
                                        labelText: "Enter invitation link",
                                      ),
                                      validator: (link) =>
                                          link!.isEmpty ? 'Enter link' : null,
                                    )),
                                width: 400,
                                padding: const EdgeInsets.fromLTRB(
                                    10.0, 20.0, 10.0, 10.0),
                              ),
                              Row(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(
                                        38.0, 110.0, 0.0, 0.0),
                                    child: ElevatedButton(
                                        style: ElevatedButton.styleFrom(
                                            padding: const EdgeInsets.symmetric(
                                                vertical: 24, horizontal: 24),
                                            textStyle: Theme.of(context)
                                                .textTheme
                                                .bodyText2!
                                                .copyWith(
                                                    color: styles.themeColors
                                                        .background)),
                                        onPressed: () {
                                          if (_formKey.currentState!
                                              .validate()) {
                                            currentUser.state = mockUser;
                                            Navigator.pushReplacement(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        ConfVideoView()));
                                          }
                                        },
                                        child: const Text("Log In")),
                                  ),
                                ],
                              ),
                            ])),
                  ]),
            )));
  }
}
