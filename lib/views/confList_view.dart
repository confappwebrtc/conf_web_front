import 'package:fixnum/fixnum.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:webrtc_conf/generated/proto/user.pb.dart';
import 'package:webrtc_conf/providers/conf_provider.dart';
import 'package:webrtc_conf/providers/schedule_provider.dart';
import 'package:webrtc_conf/providers/user_provider.dart';
import 'package:webrtc_conf/styles.dart';
import 'package:webrtc_conf/layout.dart';
import 'package:webrtc_conf/generated/proto/conference.pb.dart';
import 'package:webrtc_conf/views/confAll_view.dart';
import 'package:webrtc_conf/views/panel_view.dart';

class ConfListView extends ConsumerStatefulWidget {
  const ConfListView({Key? key}) : super(key: key);

  @override
  ConsumerState<ConfListView> createState() => _ConfListViewState();
}

class _ConfListViewState extends ConsumerState<ConfListView> {
  @override
  Widget build(BuildContext context) {
    final provider = ref.watch(conferenceListProvider);
    final currentUser = ref.watch(userProvider.state);
    return MainLayout(
        child: Material(
            child: Scaffold(
                body: ListView(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 20, vertical: 15)
                            .copyWith(left: 15),
                    children: [
                      provider.when(
                          data: (confList) {
                            return Wrap(
                                children: confList.map((element) {
                              return Container(
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15)),
                                child: ConferenceTile(element, onPressed: () {
                                  setState(() {
                                    Navigator.pushReplacement(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => PanelView()));
                                  });
                                }),
                              );
                            }).toList());
                          },
                          error: (e, s) => Text(e.toString()),
                          loading: () => SizedBox.square(
                              dimension: 150, child: styles.loadingAnimation))
                    ]),
                extendBodyBehindAppBar: true,
                extendBody: true,
                bottomNavigationBar: Container(
                  color: styles.themeColors.background.withOpacity(0.0),
                  child:
                      Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                    ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            padding: const EdgeInsets.symmetric(
                                vertical: 20, horizontal: 11),
                            textStyle: Theme.of(context)
                                .textTheme
                                .bodyText2!
                                .copyWith(color: styles.themeColors.background),
                            side: BorderSide(
                                width: 2, color: styles.themeColors.autoBorder),
                            primary: styles.themeColors.background),
                        onPressed: () {
                          currentUser.state = mockUser;
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ConfAllView()));
                        },
                        child: const Text("All conferences",
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.black))),
                    const SizedBox(width: 15),
                  ]),
                ))));
  }
}

class ConferenceTile extends ConsumerWidget {
  final Conference conf;
  final VoidCallback onPressed;

  const ConferenceTile(this.conf, {Key? key, required this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final provider = ref.watch(conferenceListProvider);
    final schedule = ref.watch(groupedSchedule(conf));
    return Material(
      color: styles.themeColors.brand.withOpacity(0),
      borderRadius: BorderRadius.circular(15),
      clipBehavior: Clip.antiAlias,
      child: InkWell(
        onTap: onPressed,
        child: Padding(
          padding: const EdgeInsets.all(25),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                  margin: const EdgeInsets.fromLTRB(25.0, 40.0, 25.0, 40.0),
                  height: 187.28,
                  width: 300,
                  foregroundDecoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(
                          color: styles.themeColors.brand, width: 2)),
                  child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: styles.themeColors.brand))),
              Container(
                margin: const EdgeInsets.fromLTRB(50.0, 0.0, 50.0, 0.0),
                padding: const EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 50.0),
                foregroundDecoration: BoxDecoration(
                    border: Border(
                        left: BorderSide(
                            width: 5.0, color: styles.themeColors.brand))),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(children: [
                      Container(
                        width: 400,
                        padding: const EdgeInsets.only(bottom: 15.0),
                        child: Text(
                          conf.title,
                          style: TextStyle(color: Colors.blue, fontSize: 28),
                        ),
                      ),
                      Container(
                          width: 400,
                          padding: const EdgeInsets.only(bottom: 15.0),
                          child: Row(children: [
                            Text(
                                DateFormat.yMMMd().format(
                                    DateTime.fromMillisecondsSinceEpoch(
                                        conf.starts.toInt())),
                                style: Theme.of(context).textTheme.headline5),
                            Text(' - ',
                                style: Theme.of(context).textTheme.headline5),
                            Text(
                                DateFormat.yMMMd().format(
                                    DateTime.fromMillisecondsSinceEpoch(
                                        conf.ends.toInt())),
                                style: Theme.of(context).textTheme.headline5)
                          ])),
                      Container(
                          width: 400,
                          padding: const EdgeInsets.only(bottom: 15.0),
                          child: Row(
                            children: [
                              Padding(
                                  padding: const EdgeInsets.only(right: 20.0),
                                  child: Text("Section",
                                      style: TextStyle(color: Colors.grey))),
                              Container(
                                  margin: const EdgeInsets.only(right: 15.0),
                                  decoration: BoxDecoration(
                                      color: Color.fromRGBO(140, 209, 255, 1),
                                      borderRadius: BorderRadius.circular(8)),
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 5.0, horizontal: 20.0),
                                  child: Text(
                                    "IOS",
                                    style: TextStyle(color: Colors.white),
                                  )),
                              Container(
                                  margin: const EdgeInsets.only(right: 15.0),
                                  decoration: BoxDecoration(
                                      color: Color.fromRGBO(78, 255, 78, 1),
                                      borderRadius: BorderRadius.circular(8)),
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 5.0, horizontal: 20.0),
                                  child: Text("Android",
                                      style: TextStyle(color: Colors.white))),
                              Container(
                                  margin: const EdgeInsets.only(right: 15.0),
                                  decoration: BoxDecoration(
                                      color: Color.fromRGBO(255, 168, 65, 1),
                                      borderRadius: BorderRadius.circular(8)),
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 5.0, horizontal: 20.0),
                                  child: Text("Web",
                                      style: TextStyle(color: Colors.white)))
                            ],
                          )),
                    ]),
                    Container(
                        width: 400,
                        child: Column(children: [
                          Container(
                              width: 400,
                              alignment: Alignment.centerLeft,
                              padding: EdgeInsets.only(bottom: 20.0),
                              child: Row(children: [
                                Container(
                                    child: Column(children: [
                                  Text('09:00',
                                      style: Theme.of(context)
                                          .textTheme
                                          .subtitle2!
                                          .copyWith(
                                              color:
                                                  styles.themeColors.caption)),
                                  Text('-',
                                      style: Theme.of(context)
                                          .textTheme
                                          .subtitle2!
                                          .copyWith(
                                              color:
                                                  styles.themeColors.caption)),
                                  Text('10:30',
                                      style: Theme.of(context)
                                          .textTheme
                                          .subtitle2!
                                          .copyWith(
                                              color:
                                                  styles.themeColors.caption)),
                                ])),
                                Container(
                                  width: 320,
                                  alignment: Alignment.centerLeft,
                                  margin: EdgeInsets.only(left: 30.0),
                                  padding: const EdgeInsets.fromLTRB(
                                      20.0, 0.0, 0.0, 0.0),
                                  foregroundDecoration: BoxDecoration(
                                      border: Border(
                                          left: BorderSide(
                                              width: 3.0,
                                              color: Color.fromRGBO(
                                                  140, 209, 255, 1)))),
                                  child: Column(
                                    children: [
                                      Container(
                                          width: 320,
                                          alignment: Alignment.centerLeft,
                                          padding: EdgeInsets.symmetric(
                                              vertical: 5.0, horizontal: 0.0),
                                          child: Text(
                                              'Swift - the new language for iOS coding',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyText1!
                                                  .copyWith(
                                                      color: styles
                                                          .themeColors.caption),
                                              softWrap: true)),
                                      Container(
                                          width: 320,
                                          alignment: Alignment.centerLeft,
                                          padding: EdgeInsets.symmetric(
                                              vertical: 5.0),
                                          child: Text('Tatiana Kalekina',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyText2!
                                                  .copyWith(
                                                      color: styles.themeColors
                                                          .autoBorder,
                                                      fontWeight:
                                                          FontWeight.w600))),
                                    ],
                                  ),
                                )
                              ])),
                          Container(
                              width: 400,
                              alignment: Alignment.centerLeft,
                              padding: EdgeInsets.only(bottom: 20.0),
                              child: Row(children: [
                                Container(
                                    child: Column(children: [
                                  Text('11:00',
                                      style: Theme.of(context)
                                          .textTheme
                                          .subtitle2!
                                          .copyWith(
                                              color:
                                                  styles.themeColors.caption)),
                                  Text('-',
                                      style: Theme.of(context)
                                          .textTheme
                                          .subtitle2!
                                          .copyWith(
                                              color:
                                                  styles.themeColors.caption)),
                                  Text('12:30',
                                      style: Theme.of(context)
                                          .textTheme
                                          .subtitle2!
                                          .copyWith(
                                              color:
                                                  styles.themeColors.caption)),
                                ])),
                                Container(
                                  width: 320,
                                  alignment: Alignment.centerLeft,
                                  margin: EdgeInsets.only(left: 30.0),
                                  padding: const EdgeInsets.fromLTRB(
                                      20.0, 0.0, 0.0, 0.0),
                                  foregroundDecoration: BoxDecoration(
                                      border: Border(
                                          left: BorderSide(
                                              width: 3.0,
                                              color: Color.fromRGBO(
                                                  78, 255, 78, 1)))),
                                  child: Column(
                                    children: [
                                      Container(
                                          width: 320,
                                          alignment: Alignment.centerLeft,
                                          padding: EdgeInsets.symmetric(
                                              vertical: 5.0, horizontal: 0.0),
                                          child: Text('Objective C vs. Swift',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyText1!
                                                  .copyWith(
                                                      color: styles
                                                          .themeColors.caption),
                                              softWrap: true)),
                                      Container(
                                          width: 320,
                                          alignment: Alignment.centerLeft,
                                          padding: EdgeInsets.symmetric(
                                              vertical: 5.0),
                                          child: Text('Anastasia Roshchina',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyText2!
                                                  .copyWith(
                                                      color: styles.themeColors
                                                          .autoBorder,
                                                      fontWeight:
                                                          FontWeight.w600))),
                                    ],
                                  ),
                                )
                              ])),
                          Container(
                              width: 400,
                              alignment: Alignment.centerLeft,
                              padding: EdgeInsets.only(bottom: 20.0),
                              child: Row(children: [
                                Container(
                                    child: Column(children: [
                                  Text('13:00',
                                      style: Theme.of(context)
                                          .textTheme
                                          .subtitle2!
                                          .copyWith(
                                              color:
                                                  styles.themeColors.caption)),
                                  Text('-',
                                      style: Theme.of(context)
                                          .textTheme
                                          .subtitle2!
                                          .copyWith(
                                              color:
                                                  styles.themeColors.caption)),
                                  Text('14:30',
                                      style: Theme.of(context)
                                          .textTheme
                                          .subtitle2!
                                          .copyWith(
                                              color:
                                                  styles.themeColors.caption)),
                                ])),
                                Container(
                                  width: 320,
                                  alignment: Alignment.centerLeft,
                                  margin: EdgeInsets.only(left: 30.0),
                                  padding: const EdgeInsets.fromLTRB(
                                      20.0, 0.0, 0.0, 0.0),
                                  foregroundDecoration: BoxDecoration(
                                      border: Border(
                                          left: BorderSide(
                                              width: 3.0,
                                              color: Color.fromRGBO(
                                                  255, 168, 65, 1)))),
                                  child: Column(
                                    children: [
                                      Container(
                                          width: 320,
                                          alignment: Alignment.centerLeft,
                                          padding: EdgeInsets.symmetric(
                                              vertical: 5.0, horizontal: 0.0),
                                          child: Text(
                                              'How to fall in love with iOS',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyText1!
                                                  .copyWith(
                                                      color: styles
                                                          .themeColors.caption),
                                              softWrap: true)),
                                      Container(
                                          width: 320,
                                          alignment: Alignment.centerLeft,
                                          padding: EdgeInsets.symmetric(
                                              vertical: 5.0),
                                          child: Text('Salem Ajamia',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyText2!
                                                  .copyWith(
                                                      color: styles.themeColors
                                                          .autoBorder,
                                                      fontWeight:
                                                          FontWeight.w600))),
                                    ],
                                  ),
                                )
                              ])),
                        ]))
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
