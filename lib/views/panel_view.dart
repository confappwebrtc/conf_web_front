import 'package:fixnum/fixnum.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:webrtc_conf/providers/conf_provider.dart';
import 'package:webrtc_conf/providers/user_provider.dart';
import 'package:webrtc_conf/styles.dart';
import 'package:webrtc_conf/layout.dart';
import 'package:webrtc_conf/views/confList_view.dart';
import 'package:webrtc_conf/views/logLink_view.dart';

class PanelView extends ConsumerStatefulWidget {
  const PanelView({Key? key}) : super(key: key);

  @override
  ConsumerState<PanelView> createState() => _PanelViewState();
}

class _PanelViewState extends ConsumerState<PanelView> {
  @override
  Widget build(BuildContext context) {
    final conf = ref.watch(selectedConference);
    final user = ref.watch(userProvider.state);
    return MainLayout(
      child: Container(
        padding: const EdgeInsets.fromLTRB(25.0, 5.0, 0.0, 25.0),
        child: Column(children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 20.0),
                child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        padding: const EdgeInsets.symmetric(
                            vertical: 15, horizontal: 1),
                        textStyle: Theme.of(context)
                            .textTheme
                            .bodyText2!
                            .copyWith(color: styles.themeColors.textSecondary),
                        primary: styles.themeColors.background,
                        side: BorderSide(
                            width: 2, color: styles.themeColors.autoBorder)),
                    onPressed: () {
                      user.state = mockUser;
                      Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ConfListView()));
                    },
                    child: const Text("Conferences",
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.black))),
              ),
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                  margin: const EdgeInsets.fromLTRB(25.0, 0.0, 25.0, 40.0),
                  height: 187.28,
                  width: 300,
                  foregroundDecoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(
                          color: styles.themeColors.brand, width: 2)),
                  child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: styles.themeColors
                              .brand /*CachedNetworkImage(
                    imageUrl: conf.imageUrl,
                    fit: BoxFit.cover,
                    imageBuilder: (context, provider) {
                      return Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              image: DecorationImage(
                                  image: provider, fit: BoxFit.cover)));
                    },
                  )*/
                          ))),
              Container(
                  margin: const EdgeInsets.fromLTRB(50.0, 0.0, 50.0, 0.0),
                  height: 280,
                  child: Column(children: [
                    Container(
                      width: 800,
                      padding: const EdgeInsets.only(bottom: 15.0),
                      child: Text(
                        conf.title,
                        style: TextStyle(color: Colors.blue, fontSize: 28),
                      ),
                    ),
                    Container(
                        width: 800,
                        padding: const EdgeInsets.only(bottom: 15.0),
                        child: Row(children: [
                          Text(
                              DateFormat.yMMMd().format(
                                  DateTime.fromMillisecondsSinceEpoch(
                                      conf.starts.toInt())),
                              style: Theme.of(context).textTheme.headline5),
                          Text(' - ',
                              style: Theme.of(context).textTheme.headline5),
                          Text(
                              DateFormat.yMMMd().format(
                                  DateTime.fromMillisecondsSinceEpoch(
                                      conf.ends.toInt())),
                              style: Theme.of(context).textTheme.headline5)
                        ])),
                    Container(
                        width: 800,
                        padding: const EdgeInsets.only(bottom: 15.0),
                        child: Row(
                          children: [
                            Padding(
                                padding: const EdgeInsets.only(right: 20.0),
                                child: Text("Section",
                                    style: TextStyle(color: Colors.grey))),
                            Container(
                                margin: const EdgeInsets.only(right: 15.0),
                                decoration: BoxDecoration(
                                    color: Colors.grey,
                                    borderRadius: BorderRadius.circular(8)),
                                padding: const EdgeInsets.symmetric(
                                    vertical: 5.0, horizontal: 20.0),
                                child: Text(
                                  "IOS",
                                  style: TextStyle(color: Colors.white),
                                )),
                            Container(
                                margin: const EdgeInsets.only(right: 15.0),
                                decoration: BoxDecoration(
                                    color: Colors.grey,
                                    borderRadius: BorderRadius.circular(8)),
                                padding: const EdgeInsets.symmetric(
                                    vertical: 5.0, horizontal: 20.0),
                                child: Text("Android",
                                    style: TextStyle(color: Colors.white))),
                            Container(
                                margin: const EdgeInsets.only(right: 15.0),
                                decoration: BoxDecoration(
                                    color: Colors.grey,
                                    borderRadius: BorderRadius.circular(8)),
                                padding: const EdgeInsets.symmetric(
                                    vertical: 5.0, horizontal: 20.0),
                                child: Text("Web",
                                    style: TextStyle(color: Colors.white)))
                          ],
                        )),
                    Container(
                      width: 800,
                      padding: const EdgeInsets.only(bottom: 20.0),
                      child: Text((conf.description),
                          style: Theme.of(context).textTheme.subtitle1),
                    ),
                    Container(
                        padding: const EdgeInsets.only(left: 470.0),
                        child: Row(children: [
                          Padding(
                            padding: const EdgeInsets.only(right: 20.0),
                            child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 24, horizontal: 15),
                                    textStyle: Theme.of(context)
                                        .textTheme
                                        .bodyText2!
                                        .copyWith(
                                            color: styles
                                                .themeColors.textSecondary),
                                    primary: styles.themeColors.background,
                                    side: BorderSide(
                                        width: 2,
                                        color: styles.themeColors.autoBorder)),
                                onPressed: () {},
                                child: const Text("Agenda",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(color: Colors.black))),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 20.0),
                            child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 24, horizontal: 15),
                                    textStyle: Theme.of(context)
                                        .textTheme
                                        .bodyText2!
                                        .copyWith(
                                            color: styles
                                                .themeColors.textSecondary),
                                    primary: styles.themeColors.background,
                                    side: BorderSide(
                                        width: 2,
                                        color: styles.themeColors.autoBorder)),
                                onPressed: () {
                                  user.state = mockUser;
                                  Navigator.pushReplacement(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => LogLinkView()));
                                },
                                child: const Text("Check In",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(color: Colors.black))),
                          )
                        ]))
                  ]))
            ],
          ),
          Container(
              margin: const EdgeInsets.only(top: 25.0),
              height: 310,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                      height: 310,
                      width: 240.81,
                      margin: const EdgeInsets.only(right: 70.0),
                      foregroundDecoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          border: Border.all(
                              color: styles.themeColors.cardSecondBackground
                                  .withOpacity(0.4),
                              width: 2)),
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: styles.themeColors.cardSecondBackground
                                .withOpacity(0.4)),
                        child: Column(
                          children: [
                            Container(
                                margin: const EdgeInsets.fromLTRB(
                                    25.0, 30.0, 25.0, 10.0),
                                height: 110,
                                width: 110,
                                foregroundDecoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(100),
                                    border: Border.all(
                                        color: styles.themeColors.brand,
                                        width: 2)),
                                child: Container(
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(100),
                                        color: styles.themeColors
                                            .brand /*CachedNetworkImage(
                    imageUrl: conf.imageUrl,
                    fit: BoxFit.cover,
                    imageBuilder: (context, provider) {
                      return Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              image: DecorationImage(
                                  image: provider, fit: BoxFit.cover)));
                    },
                  )*/
                                        ))),
                            Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: const Text('Tatyana Kalekina',
                                  style:
                                      TextStyle(fontWeight: FontWeight.w600)),
                            ),
                            Expanded(
                                child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 5.0, horizontal: 10.0),
                                    child: const Text(
                                        'The art of communication, or how creativity can survive in IT. Communication games based on real examples',
                                        textAlign: TextAlign.center)))
                          ],
                        ),
                      )),
                  Container(
                      height: 310,
                      width: 240.81,
                      margin: const EdgeInsets.only(right: 70.0),
                      foregroundDecoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          border: Border.all(
                              color: styles.themeColors.cardSecondBackground
                                  .withOpacity(0.4),
                              width: 2)),
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: styles.themeColors.cardSecondBackground
                                .withOpacity(0.4)),
                        child: Column(
                          children: [
                            Container(
                                margin: const EdgeInsets.fromLTRB(
                                    25.0, 30.0, 25.0, 10.0),
                                height: 110,
                                width: 110,
                                foregroundDecoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(100),
                                    border: Border.all(
                                        color: styles.themeColors.brand,
                                        width: 2)),
                                child: Container(
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(100),
                                        color: styles.themeColors
                                            .brand /*CachedNetworkImage(
                    imageUrl: conf.imageUrl,
                    fit: BoxFit.cover,
                    imageBuilder: (context, provider) {
                      return Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              image: DecorationImage(
                                  image: provider, fit: BoxFit.cover)));
                    },
                  )*/
                                        ))),
                            Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: const Text('Salem Ajamia',
                                  style:
                                      TextStyle(fontWeight: FontWeight.w600)),
                            ),
                            Expanded(
                                child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 5.0, horizontal: 10.0),
                                    child: const Text(
                                        'How a mobile developer can make a startup without a team. MVP or prototype of a whole project with a server and frontend',
                                        textAlign: TextAlign.center)))
                          ],
                        ),
                      )),
                  Container(
                      height: 310,
                      width: 240.81,
                      margin: const EdgeInsets.only(right: 70.0),
                      foregroundDecoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          border: Border.all(
                              color: styles.themeColors.cardSecondBackground
                                  .withOpacity(0.4),
                              width: 2)),
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: styles.themeColors.cardSecondBackground
                                .withOpacity(0.4)),
                        child: Column(
                          children: [
                            Container(
                                margin: const EdgeInsets.fromLTRB(
                                    25.0, 30.0, 25.0, 10.0),
                                height: 110,
                                width: 110,
                                foregroundDecoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(100),
                                    border: Border.all(
                                        color: styles.themeColors.brand,
                                        width: 2)),
                                child: Container(
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(100),
                                        color: styles.themeColors
                                            .brand /*CachedNetworkImage(
                    imageUrl: conf.imageUrl,
                    fit: BoxFit.cover,
                    imageBuilder: (context, provider) {
                      return Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              image: DecorationImage(
                                  image: provider, fit: BoxFit.cover)));
                    },
                  )*/
                                        ))),
                            Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: const Text('Anastasia Roshchina',
                                  style:
                                      TextStyle(fontWeight: FontWeight.w600)),
                            ),
                            Expanded(
                                child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 5.0, horizontal: 10.0),
                                    child: const Text(
                                        'The process of working on the design of a digital product: Goal setting, task definition, solution, launch into the store and life after launch.',
                                        textAlign: TextAlign.center)))
                          ],
                        ),
                      )),
                  Container(
                      height: 310,
                      width: 240.81,
                      foregroundDecoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          border: Border.all(
                              color: styles.themeColors.cardSecondBackground
                                  .withOpacity(0.4),
                              width: 2)),
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: styles.themeColors.cardSecondBackground
                                .withOpacity(0.4)),
                        child: Column(
                          children: [
                            Container(
                                margin: const EdgeInsets.fromLTRB(
                                    25.0, 30.0, 25.0, 10.0),
                                height: 110,
                                width: 110,
                                foregroundDecoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(100),
                                    border: Border.all(
                                        color: styles.themeColors.brand,
                                        width: 2)),
                                child: Container(
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(100),
                                        color: styles.themeColors
                                            .brand /*CachedNetworkImage(
                    imageUrl: conf.imageUrl,
                    fit: BoxFit.cover,
                    imageBuilder: (context, provider) {
                      return Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              image: DecorationImage(
                                  image: provider, fit: BoxFit.cover)));
                    },
                  )*/
                                        ))),
                            Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: const Text('Ivan Petrov',
                                  style:
                                      TextStyle(fontWeight: FontWeight.w600)),
                            ),
                            Expanded(
                                child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 5.0, horizontal: 10.0),
                                    child: const Text(
                                        'Optimize development processes and application parameters',
                                        textAlign: TextAlign.center)))
                          ],
                        ),
                      ))
                ],
              ))
        ]),
      ),
    );
  }
}
