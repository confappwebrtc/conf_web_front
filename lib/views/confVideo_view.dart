import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:webrtc_conf/layout.dart';
import 'package:webrtc_conf/providers/conf_provider.dart';
import 'package:webrtc_conf/views/confList_view.dart';

import '../styles.dart';
import '../widgets/video_block.dart';

class ConfVideoView extends ConsumerStatefulWidget {
  const ConfVideoView({Key? key}) : super(key: key);

  @override
  _ConfVideoViewState createState() => _ConfVideoViewState();
}

class _ConfVideoViewState extends ConsumerState<ConfVideoView> {
  Color selectedColor = styles.themeColors.brand;
  Color unselectedColor = styles.themeColors.caption;

  IconData volumeIcon = Icons.volume_up_rounded;
  IconData videoIcon = Icons.videocam_rounded;

  bool volumeOn = false;
  bool videoOn = false;

  @override
  Widget build(BuildContext context) {
    final conf = ref.watch(selectedConference);
    return MainLayout(
        child: Scaffold(
      extendBodyBehindAppBar: true,
      extendBody: true,
      body: SingleChildScrollView(
          child: Row(children: [
        Container(
            width: MediaQuery.of(context).size.width * 0.75,
            padding: const EdgeInsets.fromLTRB(60.0, 0.0, 20.0, 0.0),
            child: Column(children: [
              Stack(alignment: Alignment.topCenter, children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(25.0, 25.0, 25.0, 0.0),
                  child: Center(child: VideoBlock()),
                ),
                Container(
                  margin: const EdgeInsets.only(top: 25),
                  alignment: Alignment.center,
                  height: 50,
                  width: MediaQuery.of(context).size.width * 0.4,
                  decoration: BoxDecoration(
                      color: styles.themeColors.cardBackground.withOpacity(0.8),
                      borderRadius: BorderRadius.circular(100)),
                  child: Text(
                    conf.title,
                    style: const TextStyle(color: Colors.white, fontSize: 28),
                  ),
                )
              ]),
              Container(
                height: 50,
                width: MediaQuery.of(context).size.width * 0.5,
                decoration: BoxDecoration(
                    color: styles.themeColors.cardBackground,
                    borderRadius: BorderRadius.circular(100)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    IconButton(
                        onPressed: () {
                          setState(() {
                            volumeOn = !volumeOn;
                            if (volumeOn) {
                              volumeIcon = Icons.volume_off_rounded;
                            } else {
                              volumeIcon = Icons.volume_up_rounded;
                            }
                          });
                        },
                        icon: Icon(volumeIcon,
                            color: volumeOn
                                ? styles.themeColors.caption
                                : styles.themeColors.brand)),
                    IconButton(
                        onPressed: () {
                          setState(() {
                            videoOn = !videoOn;
                            if (videoOn) {
                              videoIcon = Icons.videocam_off_rounded;
                            } else {
                              videoIcon = Icons.videocam_rounded;
                            }
                          });
                        },
                        icon: Icon(videoIcon,
                            color: videoOn
                                ? styles.themeColors.caption
                                : styles.themeColors.brand)),
                    IconButton(
                        onPressed: () {},
                        icon: const Icon(Icons.screen_share_rounded)),
                    IconButton(
                        onPressed: () {
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ConfListView()));
                        },
                        icon: const Icon(Icons.call_end_rounded,
                            color: Colors.red)),
                  ],
                ),
              ),
              Row(
                children: [
                  Container(
                      width: 100,
                      padding: const EdgeInsets.fromLTRB(0.0, 10.0, 15.0, 10.0),
                      child: Text('Section',
                          style: TextStyle(
                            color: styles.themeColors.brand,
                          ))),
                  Container(
                      margin: const EdgeInsets.fromLTRB(0.0, 10.0, 15.0, 10.0),
                      decoration: BoxDecoration(
                          color: styles.themeColors.brand,
                          borderRadius: BorderRadius.circular(8)),
                      padding: const EdgeInsets.symmetric(
                          vertical: 5.0, horizontal: 20.0),
                      child: Text(
                        'IOS',
                        style: TextStyle(color: styles.themeColors.background),
                      )),
                ],
              ),
              Row(
                children: [
                  Container(
                      width: 100,
                      padding: const EdgeInsets.fromLTRB(0.0, 10.0, 15.0, 10.0),
                      child: Text('Speaker',
                          style: TextStyle(
                            color: styles.themeColors.brand,
                          ))),
                  Text('Anastasia Roshchina',
                      style: TextStyle(fontWeight: FontWeight.w600)),
                ],
              ),
              Row(
                children: [
                  Container(
                      width: 100,
                      padding: const EdgeInsets.fromLTRB(0.0, 10.0, 15.0, 50.0),
                      child: Text('Description',
                          style: TextStyle(
                            color: styles.themeColors.brand,
                          ))),
                  Expanded(child: Text(conf.description)),
                ],
              )
            ])),
        Container(
            width: MediaQuery.of(context).size.width * 0.21,
            height: MediaQuery.of(context).size.height,
            alignment: Alignment.topRight,
            child: Column(children: [
              Padding(
                  padding: const EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 25.0),
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        padding: const EdgeInsets.symmetric(
                            vertical: 20, horizontal: 11),
                        textStyle: Theme.of(context)
                            .textTheme
                            .bodyText2!
                            .copyWith(color: styles.themeColors.background),
                        side: BorderSide(
                            width: 2, color: styles.themeColors.brand),
                        primary: styles.themeColors.brand,
                      ),
                      onPressed: () {
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ConfListView()));
                      },
                      child: const Text("Conferences",
                          textAlign: TextAlign.center))),
              Container(
                  alignment: Alignment.bottomCenter,
                  height: MediaQuery.of(context).size.width * 0.32,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      color: styles.themeColors.cardSecondBackground,
                      borderRadius: BorderRadius.circular(20)),
                  child: Row(children: [
                    Container(
                      child: Form(
                          child: TextFormField(
                        inputFormatters: [
                          LengthLimitingTextInputFormatter(100)
                        ],
                        decoration: InputDecoration(
                          hintText: "Enter text...",
                          suffixIcon: IconButton(
                            icon: Icon(
                              Icons.send,
                              color: styles.themeColors.background,
                            ),
                            onPressed: () {
                              setState(() {});
                            },
                          ),
                          hintStyle: Theme.of(context)
                              .textTheme
                              .caption!
                              .copyWith(color: styles.themeColors.background),
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(4),
                              borderSide: BorderSide(
                                  color: styles.themeColors.cardBackground)),
                        ),
                      )),
                      width: MediaQuery.of(context).size.width * 0.19,
                      margin: const EdgeInsets.fromLTRB(10.0, 20.0, 10.0, 10.0),
                    ),
                  ]))
            ]))
      ])),
    ));
  }
}
