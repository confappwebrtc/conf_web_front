import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:webrtc_conf/layout.dart';
import 'package:webrtc_conf/providers/user_provider.dart';
import 'package:webrtc_conf/providers/conf_provider.dart';
import 'package:webrtc_conf/styles.dart';
import 'package:webrtc_conf/providers/common.dart';
import 'package:webrtc_conf/providers/schedule_provider.dart';

class AgendaView extends ConsumerStatefulWidget {
  final String? id;
  const AgendaView({Key? key, this.id}) : super(key: key);

  @override
  _AgendaViewState createState() => _AgendaViewState();
}

class _AgendaViewState extends ConsumerState<AgendaView> {
  @override
  Widget build(BuildContext context) {
    final user = ref.watch(userProvider.state);
    final conf = ref.watch(selectedConference);
    final panel = ref.watch(sectionsProvider(conf.id));
    final serverConference =
        ref.watch(oneConfProvider(SingleItem(conf.id, widget.id ?? "")));
    return MainLayout(
      child: CustomScrollView(slivers: <Widget>[
        SliverAppBar(
            pinned: true,
            backgroundColor: styles.themeColors.background,
            expandedHeight: 80.0,
            flexibleSpace: FlexibleSpaceBar(
                title: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                  Container(
                      margin: const EdgeInsets.only(right: 30.0),
                      child: Text(conf.title,
                          style: const TextStyle(
                              color: Colors.black, fontSize: 18))),
                  const FilterWidget()
                ])))
      ]),
    );
  }
}

class FilterWidget extends StatefulWidget {
  const FilterWidget({Key? key}) : super(key: key);

  @override
  State<FilterWidget> createState() => _FilterWidgetState();
}

class _FilterWidgetState extends State<FilterWidget> {
  String dropdownValue = 'IOS';

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: 30,
        child: DecoratedBox(
            decoration: BoxDecoration(
              border: Border.all(color: Colors.black38, width: 1),
              borderRadius: BorderRadius.circular(5),
            ),
            child: DropdownButton<String>(
              value: dropdownValue,
              alignment: AlignmentDirectional.center,
              dropdownColor: styles.themeColors.background,
              onChanged: (String? newValue) {
                setState(() {
                  dropdownValue = newValue!;
                });
              },
              items: <String>['IOS', 'Android', 'Web']
                  .map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 12.0, vertical: 5.0),
                      child: Text(
                        value,
                        style: Theme.of(context)
                            .textTheme
                            .caption!
                            .copyWith(color: styles.themeColors.text),
                        textAlign: TextAlign.center,
                      )),
                );
              }).toList(),
            )));
  }
}
