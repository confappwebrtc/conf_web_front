import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:webrtc_conf/providers/conf_provider.dart';
import 'package:webrtc_conf/styles.dart';
import 'package:webrtc_conf/layout.dart';
import 'package:webrtc_conf/generated/proto/conference.pb.dart';
import 'package:webrtc_conf/views/panel_view.dart';

class ConfAllView extends ConsumerStatefulWidget {
  const ConfAllView({Key? key}) : super(key: key);

  @override
  ConsumerState<ConfAllView> createState() => _ConfAllViewState();
}

class _ConfAllViewState extends ConsumerState<ConfAllView> {
  @override
  Widget build(BuildContext context) {
    final provider = ref.watch(conferenceListProvider);
    return MainLayout(
        child: Material(
            child: Scaffold(
                body: ListView(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 20, vertical: 15)
                            .copyWith(left: 15),
                    children: [
          provider.when(
              data: (confList) {
                return Wrap(
                    children: confList.map((element) {
                  return ConferenceTile(element);
                }).toList());
              },
              error: (e, s) => Text(e.toString()),
              loading: () => SizedBox.square(
                  dimension: 150, child: styles.loadingAnimation))
        ]))));
  }
}

class ConferenceTile extends ConsumerWidget {
  final Conference conf;

  const ConferenceTile(this.conf, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final provider = ref.watch(conferenceListProvider);
    return Material(
      color: styles.themeColors.brand.withOpacity(0),
      borderRadius: BorderRadius.circular(15),
      clipBehavior: Clip.antiAlias,
      child: InkWell(
        onTap: () {
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => PanelView()));
        },
        child: Padding(
          padding: const EdgeInsets.all(25),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                  margin: const EdgeInsets.fromLTRB(25.0, 40.0, 25.0, 40.0),
                  height: 187.28,
                  width: 300,
                  foregroundDecoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(
                          color: styles.themeColors.brand, width: 2)),
                  child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: styles.themeColors.brand))),
              Container(
                  margin: const EdgeInsets.fromLTRB(50.0, 0.0, 50.0, 0.0),
                  padding: const EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 0.0),
                  foregroundDecoration: BoxDecoration(
                      border: Border(
                          left: BorderSide(
                              width: 5.0, color: styles.themeColors.brand))),
                  height: 280,
                  child: Column(children: [
                    Container(
                      width: 800,
                      padding: const EdgeInsets.only(bottom: 15.0),
                      child: Text(
                        conf.title,
                        style: TextStyle(color: Colors.blue, fontSize: 28),
                      ),
                    ),
                    Container(
                        width: 800,
                        padding: const EdgeInsets.only(bottom: 15.0),
                        child: Row(children: [
                          Text(
                              DateFormat.yMMMd().format(
                                  DateTime.fromMillisecondsSinceEpoch(
                                      conf.starts.toInt())),
                              style: Theme.of(context).textTheme.headline5),
                          Text(' - ',
                              style: Theme.of(context).textTheme.headline5),
                          Text(
                              DateFormat.yMMMd().format(
                                  DateTime.fromMillisecondsSinceEpoch(
                                      conf.ends.toInt())),
                              style: Theme.of(context).textTheme.headline5)
                        ])),
                    Container(
                        width: 800,
                        padding: const EdgeInsets.only(bottom: 15.0),
                        child: Row(
                          children: [
                            Padding(
                                padding: const EdgeInsets.only(right: 20.0),
                                child: Text("Section",
                                    style: TextStyle(color: Colors.grey))),
                            Container(
                                margin: const EdgeInsets.only(right: 15.0),
                                decoration: BoxDecoration(
                                    color: Colors.grey,
                                    borderRadius: BorderRadius.circular(8)),
                                padding: const EdgeInsets.symmetric(
                                    vertical: 5.0, horizontal: 20.0),
                                child: Text(
                                  "IOS",
                                  style: TextStyle(color: Colors.white),
                                )),
                            Container(
                                margin: const EdgeInsets.only(right: 15.0),
                                decoration: BoxDecoration(
                                    color: Colors.grey,
                                    borderRadius: BorderRadius.circular(8)),
                                padding: const EdgeInsets.symmetric(
                                    vertical: 5.0, horizontal: 20.0),
                                child: Text("Android",
                                    style: TextStyle(color: Colors.white))),
                            Container(
                                margin: const EdgeInsets.only(right: 15.0),
                                decoration: BoxDecoration(
                                    color: Colors.grey,
                                    borderRadius: BorderRadius.circular(8)),
                                padding: const EdgeInsets.symmetric(
                                    vertical: 5.0, horizontal: 20.0),
                                child: Text("Web",
                                    style: TextStyle(color: Colors.white)))
                          ],
                        )),
                    Container(
                      width: 800,
                      padding: const EdgeInsets.only(bottom: 20.0),
                      child: Text((conf.description),
                          style: Theme.of(context).textTheme.subtitle1),
                    ),
                    Container(
                        padding: const EdgeInsets.only(left: 470.0),
                        child: Row(children: [
                          Padding(
                            padding: const EdgeInsets.only(right: 20.0),
                            child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 24, horizontal: 15),
                                    textStyle: Theme.of(context)
                                        .textTheme
                                        .bodyText2!
                                        .copyWith(
                                            color: styles
                                                .themeColors.textSecondary),
                                    primary: styles.themeColors.background,
                                    side: BorderSide(
                                        width: 2,
                                        color: styles.themeColors.autoBorder)),
                                onPressed: () {},
                                child: const Text("Agenda",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(color: Colors.black))),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 20.0),
                            child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 24, horizontal: 15),
                                    textStyle: Theme.of(context)
                                        .textTheme
                                        .bodyText2!
                                        .copyWith(
                                            color: styles
                                                .themeColors.textSecondary),
                                    primary: styles.themeColors.background,
                                    side: BorderSide(
                                        width: 2,
                                        color: styles.themeColors.autoBorder)),
                                onPressed: () {},
                                child: const Text("Check In",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(color: Colors.black))),
                          )
                        ]))
                  ]))
            ],
          ),
        ),
      ),
    );
  }
}
