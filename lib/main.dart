import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:webrtc_conf/styles.dart';
import 'package:webrtc_conf/views/main_view.dart';

void main() {
  runApp(const ProviderScope(child: ConfApp()));
}

class ConfApp extends StatelessWidget {
  const ConfApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Conference',
      theme: styles.mainTheme,
      home: const MainView(),
    );
  }
}
