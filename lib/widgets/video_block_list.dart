import 'package:flutter/material.dart';
import 'package:webrtc_conf/styles.dart';
import 'package:webrtc_conf/widgets/video_block.dart';

class VideoBlockList extends StatefulWidget {
  const VideoBlockList({Key? key}) : super(key: key);

  @override
  _VideoBlockListState createState() => _VideoBlockListState();
}

class _VideoBlockListState extends State<VideoBlockList> {
  late List<int> list;

  @override
  void initState() {
    super.initState();
    list = List.generate(15, (index) => index);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      clipBehavior: Clip.antiAlias,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
      ),
      foregroundDecoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                styles.themeColors.background,
                styles.themeColors.background.withOpacity(0),
                styles.themeColors.background.withOpacity(0),
                styles.themeColors.background
              ],
              stops: const [
                0,
                0.1,
                0.9,
                1
              ])),
      child: ListView.builder(
        itemBuilder: (BuildContext context, int index) {
          return Padding(
            padding: const EdgeInsets.only(bottom: 10),
            child: Card(
              child: SizedBox(
                  height: MediaQuery.of(context).size.height * 0.15,
                  child: VideoBlock()),
            ),
          );
        },
        itemCount: list.length,
      ),
    );
  }
}
