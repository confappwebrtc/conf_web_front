import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class VideoBlock extends StatefulWidget {
  VideoBlock({Key? key}) : super(key: key);

  @override
  _VideoBlockState createState() => _VideoBlockState();
}

class _VideoBlockState extends State<VideoBlock> {
  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 16 / 9,
      child: Container(
          clipBehavior: Clip.antiAlias,
          decoration: BoxDecoration(
              color: Colors.black, borderRadius: BorderRadius.circular(15)),
          child: FittedBox(child: Image.network("https://picsum.photos/1280/720"))),
    );
  }
}
