import 'package:flutter/material.dart';
import 'package:webrtc_conf/styles.dart';

class LogoWidget extends StatelessWidget {
  const LogoWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: styles.themeColors.brand,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisSize: MainAxisSize.min,
            children: [
          Icon(Icons.play_arrow_rounded, color: styles.themeColors.text, size: 30),
          const SizedBox(width: 5, height: 0,),
          Text(
            "ConfApp",
            style: Theme.of(context)
                .textTheme
                .headline3!
                .copyWith(color: styles.themeColors.text),
          )
        ]),
      ),
    );
  }
}
