import 'dart:async';
import 'package:fixnum/fixnum.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:webrtc_conf/providers/user_provider.dart';
import 'package:webrtc_conf/styles.dart';

import '../generated/proto/conference.pb.dart';
import 'common.dart';

enum ConfStatus { created, onEdit, ready, started, completed, moved, canceled }

final selectedConference = StateProvider<Conference>((ref) => Conference(
    id: Common.uuid.v4(),
    title: "IT talent starter pack",
    description:
        "A new point of attraction for IT specialists. Development of new ideas and actions. Workshops from speakers. Master classes on Android, iOS, cross-platform development, design and even soft-skills live.",
    starts: Int64(DateTime(DateTime.now().year, DateTime.now().month,
            DateTime.now().day - 1, 11)
        .millisecondsSinceEpoch),
    ends: Int64(DateTime(DateTime.now().year, DateTime.now().month,
            DateTime.now().day + 7, 11)
        .add(const Duration(minutes: 90))
        .millisecondsSinceEpoch)));

final conferenceListProvider = StreamProvider<List<Conference>>((ref) {
  final streamController = StreamController<List<Conference>>();

  final mockConfList = [
    Conference(
      id: Common.uuid.v4(),
      title: "IT talent starter pack",
      description:
          "A new point of attraction for IT specialists. Development of new ideas and actions. Workshops from speakers. Master classes on Android, iOS, cross-platform development, design and even soft-skills live.",
      starts: Int64(DateTime(DateTime.now().year, DateTime.now().month,
              DateTime.now().day - 1, 11)
          .millisecondsSinceEpoch),
      ends: Int64(DateTime(DateTime.now().year, DateTime.now().month,
              DateTime.now().day + 7, 11)
          .add(const Duration(minutes: 90))
          .millisecondsSinceEpoch),
    ),
    Conference(
      id: Common.uuid.v4(),
      title: "Successful morning start",
      description:
          "A new point of attraction for IT specialists. Development of new ideas and actions. Workshops from speakers. Master classes on Android, iOS, cross-platform development, design and even soft-skills live.",
      starts: Int64(DateTime(DateTime.now().year, DateTime.now().month,
              DateTime.now().day + 14, 11)
          .millisecondsSinceEpoch),
      ends: Int64(DateTime(DateTime.now().year, DateTime.now().month,
              DateTime.now().day + 30, 11)
          .add(const Duration(minutes: 90))
          .millisecondsSinceEpoch),
    ),
    Conference(
      id: Common.uuid.v4(),
      title: "New year conversations",
      description:
          "A new point of attraction for IT specialists. Development of new ideas and actions. Workshops from speakers. Master classes on Android, iOS, cross-platform development, design and even soft-skills live.",
      starts: Int64(DateTime(DateTime.now().year, DateTime.now().month,
              DateTime.now().day + 35, 11)
          .millisecondsSinceEpoch),
      ends: Int64(DateTime(DateTime.now().year, DateTime.now().month,
              DateTime.now().day + 40, 11)
          .add(const Duration(minutes: 90))
          .millisecondsSinceEpoch),
    ),
  ];

  streamController.add(mockConfList);

  ref.onDispose(() {
    streamController.close();
  });

  return streamController.stream;
});

final oneConfProvider =
    Provider.family<AsyncValue<Conference?>, SingleItem>((ref, item) {
  var res = const AsyncValue<Conference?>.loading();

  if (item.itemId.isEmpty) {
    return const AsyncValue<Conference?>.data(null);
  }

  ref.watch(conferenceListProvider).whenData((items) {
    res = AsyncValue<Conference?>.data(
        items.firstWhere((element) => element.id == item.confId));
  });

  return res;
});
